# Keanu Weblite aka "Convene"

* Private, instant group chat in a browser "No account, no app, no hassle!"
* Bring everyone together in one place: "The one place people can be because it doesn’t require a specific app"
" Connect just for now: "Groups are fleeting. They exist only as long as you need them, then disappear."
* Demo instance is live at https://letsconvene.im

## Features

* Standalone web client with a responsive, mobile-web tuned user interface
* Built upon the Matrix protcol, with full support for end-to-end encrypted messaging, and completely interoperable with any other Matrix client
* Progressive Web App capabilities
* Full multimedia upload and download: images, audio, video
* Built-in push-to-record voice messaging
* Quick room switcher
* Ability to create new rooms with name, topic and avatar icon
* Invite people to room using QR code or room invite link
* Quick replies or full reply to any message
* Message editing and deletion based on "Power Levels" (Moderator, Admin, etc) 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize build configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Theming
You can do simple theming by setting values in the configuration file, see below.

## Configuration file
The app loads runtime configutation from the server at "./config.json" and merges that with the default values in "assets/config.json".
    The following values can be set via the config file:

* **logo**  -   An url or base64-encoded image data url that represents the app logotype.
* **accentColor**   -   The accent color of the app UI. Use a HTML-style color value string, like "#ff0080".
* **show_status_messages**  -   Whether to show only user joins/leaves and display name updates, or the full range of room status updates. Possible values are "never" (only the above), "moderators" (moderators will see all status updates) or "always" (everyone will see all status updates). Defaults to "always".
* **maxSizeAutoDownloads** -    Attachments smaller than this will be auto downloaded. Default is 10Mb.
* **roomTypes** -   Available room types. This affects what is shown on the /create route, as well as access to routes /createroom, /createchannel and /createfiledrop. It should be an array with possible values "group_chat", "channel" or "file_drop". 

    Defaults to all values, ["group_chat", "channel", "file_drop"].

### Sticker short codes - To enable sticker short codes, follow these steps:
* Run the "create sticker config" script using "npm run create-sticker-config <path-to-sticker-packs>"
* Insert the resulting config blob into the "shortCodeStickers" value of the config file (assets/config.json)
* Rearrange order of sticker packs by editing the config blob above.

### Chat backgrounds
Chat backgrounds can be set using the **chat_backgrounds** config value. It can be set per room type, "direct", "invite" and "public". If no background is set for the current room type, the app will also check if a default "all" value has any backgrounds specified.
    Backgrounds are entered as an array. Which of the backgrounds in the array is used for a given room is calculated from the room ID, so that it is constant across the lifetime of the room.

```
chat_backgrounds: {
    "direct": ["https://example.com/dm1.png", "data:image/png;base64,yadiyada..."],
    "all": ["/default_background.png"]
}
```



### Attributions
Sounds from [Notification Sounds](https://notificationsounds.com)