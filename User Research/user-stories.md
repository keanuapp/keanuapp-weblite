**Resilience**

A free user needs a room link that works for a blocked user. The free user is loyal to the blocked brand.

A blocked user needs a brand to start from that works. Or at least a place to generate a room link.

_Idea: Maybe there's just one 'magic link' that always works. There's one site that has the 'magic link' and that's where free users talking with blocked users start._
