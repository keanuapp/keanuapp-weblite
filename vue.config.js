const CopyWebpackPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");
//const fs = require('fs')

module.exports = {
  transpileDependencies: ["vuetify"],

  publicPath: process.env.NODE_ENV === "production" ? "./" : "./",

  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      var c = require("./src/assets/config.json");
      args[0].title = c.appName;
      return args;
    });
  },

  configureWebpack: {
    devtool: "source-map",
    resolve: {
      fallback: {
        "path": require.resolve("path-browserify"),
        "crypto": require.resolve("crypto-browserify"),
        "stream": require.resolve("stream-browserify"),
        "fs": require.resolve("browserify-fs"),
        "buffer": require.resolve("buffer")
      }
    },
    plugins: [
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
      }),
      new webpack.ProvidePlugin({
        process: 'process/browser',
    }),
      new CopyWebpackPlugin({patterns: [
        {
          from: "./src/assets/config.json",
          to: "./",
        },
        {
          from: "./node_modules/@matrix-org/olm/olm.wasm",
          to: "./js/olm.wasm",
        },
      ]}),
    ],
  },

  devServer: {
    //https: true,

    /***
     * For testing notification via service worker in Mobile
     * Run your site locally with secure HTTPS using mkcert
     * https://web.dev/how-to-use-local-https/#running-your-site-locally-with-https-using-mkcert-recommended
    */
    // https: {
    //   key: fs.readFileSync('./your-local-ip-address-key.pem'),
    //   cert: fs.readFileSync('./your-local-ip-address.pem'),
    // }
  }
};
