export default {
  computed: {
    logotype() {
      if (this.$config.logo) {
        return this.$config.logo;
      }
      return require("@/assets/logo.svg");
    }
  }
}